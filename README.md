# Microservices Oauth2.0 Server v.1.0.0
> An open protocol to allow secure authorization in a simple and standard method from web, mobile and desktop applications.

[![Go Report Card](https://img.shields.io/badge/go%20report-A+-brightgreen.svg?style=flat)]()
[![GoDoc][godoc-image]][godoc-url] 

## Protocol Flow

```text
     +--------+                               +---------------+
     |        |--(A)- Authorization Request ->|   Resource    |
     |        |                               |     Owner     |
     |        |<-(B)-- Authorization Grant ---|               |
     |        |                               +---------------+
     |        |
     |        |                               +---------------+
     |        |--(C)-- Authorization Grant -->| Authorization |
     | Client |                               |     Server    |
     |        |<-(D)----- Access Token -------|               |
     |        |                               +---------------+
     |        |
     |        |                               +---------------+
     |        |--(E)----- Access Token ------>|    Resource   |
     |        |                               |     Server    |
     |        |<-(F)--- Protected Resource ---|               |
     +--------+                               +---------------+
```


## How To run
``` shell
cp .env.example .env
cd app/
docker-compose up -d
```

```shell
docker-compose exec go go run .
```

### Documentation
- **Swagger server:**
[http://localhost:8080](http://localhost:8080)
- **Swagger yaml:**
[swagger.yaml](https://lab.tog.co.id/monstergroup-solo/kita-digi/microservices-oauth2-echo/-/blob/master/docs/swagger.yaml)


[godoc-url]: https://godoc.org/github.com/go-oauth2/oauth2/v4
[godoc-image]: https://godoc.org/github.com/go-oauth2/oauth2/v4?status.svg
