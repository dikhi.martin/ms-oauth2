module api

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-oauth2/oauth2/v4 v4.4.3
	github.com/go-playground/validator/v10 v10.10.0
	github.com/go-redis/redis/v8 v8.11.4
	github.com/google/uuid v1.3.0
	github.com/iancoleman/strcase v0.2.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.1 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.10.1
	github.com/swaggo/swag v1.8.0
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	gorm.io/driver/mysql v1.3.2
	gorm.io/gorm v1.23.2
)
