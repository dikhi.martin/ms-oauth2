package routes

import (
	"net/http"

	"api/app/controller"
	"api/app/lib"
	middleware "api/app/middleware"
	oauth2 "api/app/middleware/oauth2"
	"api/app/services"
	echomiddleware "github.com/labstack/echo/middleware"

	"github.com/labstack/echo"
	"github.com/spf13/viper"
)

// Handle all request to route to controller
func Handle(app *echo.Echo) {
	services.InitRedis()
	services.InitDatabase()

	// Middleware
	app.Pre(echomiddleware.AddTrailingSlash())
	app.Use(echomiddleware.Recover())
	app.Use(echomiddleware.CORSWithConfig(echomiddleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{"*"},
		AllowMethods: []string{"*"},
	}))
	app.Use(middleware.Oauth2Authentication)
	app.Use(lib.ServerHeader)
	lib.LogMiddleware(app)

	api := app.Group(viper.GetString("ENDPOINT"))
	api.GET("/", controller.ApiIndexGet)

	// oauth route belong here
	api.POST("/token/", oauth2.HandleTokenRequest)
	api.POST("/refresh_token/", oauth2.HandleTokenRequest)
	api.Use(oauth2.TokenHandler())
	api.POST("/authenticate/", func(c echo.Context) error {
		ti := c.Get(oauth2.DefaultConfig.TokenKey)
		if ti != "" {
			return c.JSON(http.StatusOK, ti)
		}
		return echo.NewHTTPError(http.StatusNotFound, "token not found")
	})
}
