package controller

import (
	"api/app/lib"
	"github.com/labstack/echo"
	"github.com/spf13/viper"
)

// ApiIndexGet index page
// @Summary show basic response
// @Description show basic response
// @Accept  application/json
// @Produce  application/json
// @Success 200 {object} lib.Response "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Router / [get]
// @Tags Index
// ApiIndexGet func
func ApiIndexGet(c echo.Context) error {
	info := ResponseJson{
		"name":    viper.GetString("APP_NAME"),
		"version": viper.GetString("APP_VERSION"),
	}
	return lib.OK(c, info)
}
