package auth

import (
	"api/app/lib"
	"api/app/middleware"
	"github.com/labstack/echo"
)

// PostTokenRequest godoc
// @Summary Request Access Token
// @Description Request Access Token
// @Accept  multipart/form-data
// @Param grant_type query string true "Grant Type" Enums(password)
// @Param client_id formData string true "Client ID"
// @Param client_secret formData string true "Client Secret"
// @Param username formData string true "Username"
// @Param password formData string true "Password"
// @Success 200 {object} model.ResponseToken "OK"
// @Failure 401 {object} lib.Response "Unauthorized"
// @Failure 400 {object} lib.Response "Bad Request"
// @Failure 204 {object} lib.Response "No Content"
// @Failure 404 {object} lib.Response "Not Found"
// @Failure 409 {object} lib.Response "Conflict"
// @Failure 500 {object} lib.Response "Internal Server Error"
// @Router /token [post]
// @Tags Oauth2
func PostTokenRequest(c echo.Context) error {
	TokenRequest, err := middleware.GenerateAccessToken(c)
	if err != nil {
		lib.ErrorInternal(c, err.Error())
	}
	return lib.OK(c, TokenRequest)
}
