package oauth2

import (
	"sync"

	"github.com/go-oauth2/oauth2/v4"
	"github.com/go-oauth2/oauth2/v4/server"
	"github.com/labstack/echo"
)

var (
	// EServer init
	EServer *server.Server
	once    sync.Once
)

// InitServer Initialize the service
func InitServer(manager oauth2.Manager) *server.Server {
	once.Do(func() {
		EServer = server.NewDefaultServer(manager)
	})
	return EServer
}

// HandleAuthorizeRequest the authorization request handling
func HandleAuthorizeRequest(c echo.Context) error {
	return EServer.HandleAuthorizeRequest(c.Response().Writer, c.Request())
}

// HandleTokenRequest token request handling
func HandleTokenRequest(c echo.Context) error {
	return EServer.HandleTokenRequest(c.Response().Writer, c.Request())
}
