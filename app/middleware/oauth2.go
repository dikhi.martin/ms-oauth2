package middleware

import (
	"api/app/lib"
	oauth2server "api/app/middleware/oauth2"
	"api/app/model"
	"api/app/services"
	"context"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-oauth2/oauth2/v4"
	"github.com/go-oauth2/oauth2/v4/errors"
	"github.com/go-oauth2/oauth2/v4/generates"
	"github.com/go-oauth2/oauth2/v4/manage"
	"github.com/go-oauth2/oauth2/v4/server"

	"github.com/go-oauth2/oauth2/v4/store"
	"github.com/labstack/echo"
)

var logs = lib.RecordLog("SYSTEMS -")

// Oauth2Client func
type Oauth2Client struct {
	model.Oauth2Client
}

// GetID func
func (o Oauth2Client) GetID() string {
	return o.ID
}

// GetSecret func
func (o Oauth2Client) GetSecret() string {
	return o.Secret
}

// GetDomain func
func (o Oauth2Client) GetDomain() string {
	return o.Domain
}

// GetUserID func
func (o Oauth2Client) GetUserID() string {
	return o.UserID
}

// VerifyPassword func
func (o Oauth2Client) VerifyPassword(password string) bool {
	return lib.CheckPasswordHash(password, o.Secret)
}

// GetByID func
func (s *Oauth2ClientStore) GetByID(ctx context.Context, id string) (oauth2.ClientInfo, error) {
	db := services.DB
	var client Oauth2Client
	res := db.Where("id = ?", id).First(&client)
	if nil != res.Error {
		logs.Println(res.Error)
		return nil, res.Error
	}
	return client, nil
}

// GetOauth2Client func
func GetOauth2Client(id string) (Oauth2Client, error) {
	db := services.DB
	var client Oauth2Client
	res := db.Where("id = ?", id).First(&client)
	if nil != res.Error {
		logs.Println(res.Error)
		return client, res.Error
	}
	return client, nil
}

// Oauth2ClientStore type
type Oauth2ClientStore struct {
}

// Oauth2Authentication func
func Oauth2Authentication(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		manager := manage.NewDefaultManager()
		// token store
		manager.MustTokenStorage(store.NewMemoryTokenStore())
		manager.MapAccessGenerate(generates.NewJWTAccessGenerate("", []byte("00000000"), jwt.SigningMethodHS512))
		manager.SetPasswordTokenCfg(&manage.Config{
			AccessTokenExp:    time.Hour * 24,
			RefreshTokenExp:   time.Hour * 24,
			IsGenerateRefresh: true,
		})
		// clientStore initialize
		clientStore := &Oauth2ClientStore{}
		manager.MapClientStorage(clientStore)

		// Initialize the oauth2 service
		oauth2server.InitServer(manager)
		oauth2server.SetAllowGetAccessRequest(true)
		oauth2server.SetClientInfoHandler(server.ClientFormHandler)

		oauth2server.SetPasswordAuthorizationHandler(func(ctx context.Context, username, password string) (userID string, err error) {
			db := services.DB
			var user model.User
			res := db.Where("username = ? OR email = ?", username, username).First(&user)
			if nil != res.Error {
				logs.Println(err)
				err = res.Error
			} else if res.RowsAffected == 1 {
				match := lib.CheckPasswordHash(password, user.Password)
				if match {
					userID = strconv.FormatUint(uint64(user.ID), 10)
				} else {
					err = errors.ErrUnauthorizedClient
				}
			} else {
				err = errors.ErrUnauthorizedClient
			}
			return
		})

		return next(c)
	}
}

// GenerateAccessToken func
func GenerateAccessToken(c echo.Context) (oauth2.TokenInfo, error) {
	eServer := oauth2server.EServer
	ctx := context.Background()

	gt, tgr, err := eServer.ValidationTokenRequest(c.Request())
	if err != nil {
		logs.Println(err)
		return nil, err
	}
	ti, err := eServer.GetAccessToken(ctx, gt, tgr)
	if err != nil {
		logs.Println(err)
		return nil, err
	}
	return ti, nil
}

// LoadAccessToken func
func LoadAccessToken(c echo.Context) (oauth2.TokenInfo, error) {
	eServer := oauth2server.EServer
	ti, err := eServer.ValidationBearerToken(c.Request())
	if err != nil {
		logs.Println(err)
		return ti, err
	}
	return ti, nil
}
