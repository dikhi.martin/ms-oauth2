package lib

import (
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo"
)

// VALIDATOR validate request body
var VALIDATOR *validator.Validate = validator.New()

func overrideErrorMessages() {
}

// BodyParser with validation
func BodyParser(c echo.Context, payload interface{}) error {
	if err := c.Bind(payload); nil != err {
		return err
	}
	return VALIDATOR.Struct(payload)
}
