package lib

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

// ServerHeader func
func ServerHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set(echo.HeaderServer, "Oauth2/0.1")
		return next(c)
	}
}

// LogMiddleware func
func LogMiddleware(e *echo.Echo) {
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: `[${time_rfc3339}]  ${status}  ${method} ${host}${path} ${latency_human}` + "\n",
	}))
}
