package config

import "os"

// Environment variables
var Environment map[string]interface{} = map[string]interface{}{
	"app_name":                  os.Getenv("APP_NAME"),
	"app_version":               os.Getenv("APP_VERSION"),
	"app_url":                   os.Getenv("APP_URL"),
	"port":                      os.Getenv("APP_PORT"),
	"endpoint":                  os.Getenv("ENDPOINT"),
	"environment":               os.Getenv("ENVIRONTMENT"),
	"db_host":                   os.Getenv("DB_HOST"),
	"db_port":                   os.Getenv("DB_PORT"),
	"db_user":                   os.Getenv("DB_USER"),
	"db_pass":                   os.Getenv("DB_PASS"),
	"db_name":                   os.Getenv("DB_NAME"),
	"tb_name":                   os.Getenv("TB_NAME"),
	"db_table_prefix":           "",
	"redis_host":                os.Getenv("REDIS_HOST"),
	"redis_port":                os.Getenv("REDIS_PORT"),
	"redis_pass":                "",
	"redis_index":               os.Getenv("REDIS_DB"),
	"oauth2_client_name":        "oauth2",
	"oauth2_client_id":          os.Getenv("OAUTH2_CLIENT_ID"),
	"oauth2_client_secret":      os.Getenv("OAUTH2_CLIENT_SECRET"),
	"oauth2_client_grant_types": "authorization_code, password, client_credentials, refresh_token, implicit",
	"oauth2_domain":             os.Getenv("OAUTH2_DOMAIN"),
}
