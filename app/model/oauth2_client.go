package model

import (
	"api/app/lib"
	"github.com/spf13/viper"
)

// Oauth2Client models *its use to definition table GORM
type Oauth2Client struct {
	ID     string `gorm:"primaryKey;column:id;type:VARCHAR(50)"`
	Secret string `gorm:"column:client_secret;type:VARCHAR(60)"`
	Domain string `gorm:"column:redirect_uri;type:VARCHAR(60)"`
	UserID string `gorm:"column:user_id;type:VARCHAR(20)"`
}

// TableName *its use to define custom table name
func (Oauth2Client) TableName() string {
	return "oauth2_client"
}

// Seed Oauth2Client *its use to seed data
func (s *Oauth2Client) Seed() *Oauth2Client {
	data := Oauth2Client{
		ID:     viper.GetString("OAUTH2_CLIENT_ID"),
		Secret: lib.HashPassword(viper.GetString("OAUTH2_CLIENT_SECRET")),
		Domain: viper.GetString("OAUTH2_DOMAIN"),
		UserID: "1",
	}
	return &data
}
