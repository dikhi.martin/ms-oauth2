package model

import (
	"api/app/lib"
	"github.com/spf13/viper"
)

// User models *its use to definition table GORM
type User struct {
	ID       uint   `gorm:"primaryKey;column:id"`
	Username string `gorm:"column:username;type:VARCHAR(60)"`
	Email    string `gorm:"column:email;type:VARCHAR(60)"`
	Password string `gorm:"column:password;type:VARCHAR(60)"`
}

// TableName *its use to define custom table name
func (User) TableName() string {
	return viper.GetString("TB_NAME")
}

// Seed User *its use to seed data
func (s *User) Seed() *User {
	data := User{
		Username: "admin",
		Email:    "admin@mail.com",
		Password: lib.HashPassword("password"),
	}
	return &data
}
