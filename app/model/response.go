package model

// ResponseToken models
type ResponseToken struct {
	AccessToken  *string `json:"access_token,omitempty"`
	ExpiresIn    *int    `json:"expires_in,omitempty"`
	RefreshToken *string `json:"refresh_token,omitempty"`
	Scope        *string `json:"scope,omitempty"`
	TokenType    *string `json:"token_type,omitempty"`
}
