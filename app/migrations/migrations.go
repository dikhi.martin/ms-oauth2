package migrations

import "api/app/model"

// ModelMigrations models to migrate
var ModelMigrations []interface{} = []interface{}{
	&model.Oauth2Client{},
	&model.User{},
}
