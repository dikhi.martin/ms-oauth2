package migrations

import "api/app/model"

var (
	oauth2Client model.Oauth2Client
	user         model.User
)

// DataSeeds data to seeds
func DataSeeds() []interface{} {
	return []interface{}{
		oauth2Client.Seed(),
		user.Seed(),
	}
}
