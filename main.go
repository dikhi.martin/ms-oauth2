package main

// @title Authentication API
// @version 1.0.0
// @description API Documentation
// @termsOfService https://lab.tog.co.id
// @contact.name Developer
// @contact.email dikhi@tog.co.id
// @host localhost:9096
// @schemes http
// @BasePath /api/v1/oauth

// @in header
// @name Authorization
// @securitydefinitions.oauth2.password OAuth2Password
// @tokenUrl http://localhost:9096/api/v1/oauth/token
// @scope.read Grants read access
// @scope.write Grants write access
// @scope.admin Grants read and write access to administrative information
import (
	"api/app/lib"
	"api/app/routes"
	"github.com/labstack/echo"
	"os"
)

var logs = lib.RecordLog("SYSTEMS -")

func main() {
	app := echo.New()

	routes.Handle(app)
	app.Logger.Fatal(app.Start(":" + os.Getenv("APP_PORT")))
	logs.Println("Starting Application " + os.Getenv("APP_NAME"))
}
